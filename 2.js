$("noscript").each((i, tag) => {
	/* Force the <noscript> tag contents to be interpreted as HTML, as cheerio interprets them as text by default... */
	let noScriptContents = $(tag).text();
	let parsedNoScriptContents = cheerio.load(noScriptContents);
	$(tag).empty().append(parsedNoScriptContents("body").contents());
});